# Lecture_Notes
Lecture Notes for the following modules:
- **CS2DSA** - Data Structures in Algorithms using Java
- **CS2SE** - Software Engineering
- **CS2IA** - Introduction to Artificial Intelligence

## Information
Lecture Notes written in LaTeX, and are published using PDF.
