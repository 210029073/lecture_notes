\documentclass[a4paper,11pt]{article}
\usepackage[utf8]{inputenc}
\usepackage[pdftitle={Systems Analysis}, pdfauthor={Ibrahim Ahmad}, pdfsubject={Software Engineering}]{hyperref}

\usepackage{bookmark}
\usepackage{graphicx}

%opening
\title{Systems Analysis - Lecture Notes}
\author{Ibrahim Ahmad}

\begin{document}

\maketitle
 
\section{System Analysis}
Synthesis is focused most of the time, putting knowledge required based on business processes identified.
\newline\newline
Putting use-cases into technical solution for solving a problem. Though with use-cases they are not implementable, but they can be read to be implemented.
\newline\newline
Overlaps domain analysis, but is technical and synthesis activity, should there be vague details, then requirements must be refined and changed in order to be clear.

\section{The big picture}
\begin{center}
 \includegraphics[width=320pt,height=200pt]{assets/image003.png}
 % image003.png: 0x0 px, 0dpi, nanxnan cm, bb=
\end{center}

Diagram depicts use-cases during business analysis thus feeding system analysis. This potentially results to a system according to object models, which are modelled according to UML diagrams providing synthesis resulting to technical solutions from an abstract model providing abstraction which is then evolved with details.

\subsection{The role of business analysts}
Business Analyst An expert of some domain Bas, which analyses the current business system.
Be able to retrieve requirements effectively, highlight problems and feeds the systems analysis which the appropriate scope.
A system analyst is responsible of providing solutions to problems, in terms of managing the technical details according to technical knowledge for the enhancement of systems, such as advancing current systems.
Analyses use-cases and pictures them in physical model which models the actual system from objects, which are derived from objects and classes.
\begin{center}
 \includegraphics[width=360pt,height=140pt]{assets/image007.png}
 % image003.png: 0x0 px, 0dpi, nanxnan cm, bb=
\end{center}
\subsection{Steps for Business Analysis}
Step 1 we begin with identifying the business process.
Step 2 identifying the use-cases which then turn to a comprehensive model.

\section{Object-oriented system analysis}
OOSA utilises objects to depict real-life objects for dealing with complex problems in an efficient and timely manner so that we can implement software object. The idea of OOAD was dated back in the 1960s, from which was derived by a language called Simula, thus being a simulator for simulating things which resulted to a general purpose programming in the later years, for representing real-life entities into software objects.
Attriibute entities, descrives the characteristics of the entitiy.
\newline\newline
Behaviour, describing the behaviour of an object in terms of how it acts such as a student attending a lecture.
Autonomous behaviour, an object expresses itself without exchanging information from another object, such as a thread using its own private method to perform an operation without exchanging information from other threads.
Object interactions, the signal sent between the objects according to data, multiplicities and relations.
OO models, allows objects to interact by passing messages to each other.
\subsection{UML sequence diagrams}
 \begin{center}
 \includegraphics[width=320pt,height=200pt]{assets/image009.png}
 % image003.png: 0x0 px, 0dpi, nanxnan cm, bb=
\end{center}
They provide notation to draw objects and sequences of interactions between them using time frames between objects.
\newline\newline
Objects are arranged along the top of the diagram, and their interactions are listed underneath based on a time frame set along life lines of an object.
\newline\newline
Though two-dimensional diagram, it describes different interactions between objects.
Interactions can essentially be method calls, parameter passing, or by passing fields.
\newline\newline
The unit of the time does not really matter since its arbitrary.
Supports synthesis through analysis by combining the sequences in use-case descriptions.

\subsection{Objects in sequence diagram}
\subsubsection{Boundary object}
\begin{center}
 \includegraphics[width=100pt,height=150pt]{assets/image013a.jpg}
 % image013a.jpg: 0x0 px, 0dpi, nanxnan cm, bb=
\end{center}
Interacts in the outside world, often in UI and API for translating, or presenting data to actor for allowing interactions between actor and system. 
\newline\newline
Objects are usually within a system boundary, which can be internal of the system from which actor can access outside the system or from remote system. They do not need to store data in the system, however can manipulate from the user, translating data into meaningful data for the system, and preparing the data so that it can easily represented by an actor such as presentation side of things.

\subsubsection{Control Object}
\begin{center}
 \includegraphics[width=100pt,height=150pt]{assets/image015.png}
 % image013a.jpg: 0x0 px, 0dpi, nanxnan cm, bb=
\end{center}
Logic and decision making is performed on the data by control object which depends on entity object for retrieving data from them, thus interacting with the entity objects, which is behind the boundary object for sending and receiving data.

\subsubsection{Entity Objects}
\begin{center}
 \includegraphics[width=100pt,height=150pt]{assets/image017.png}
 % image013a.jpg: 0x0 px, 0dpi, nanxnan cm, bb=
\end{center}
An object that represents the concepts or entity found in real-world such as an employee class providing employee objects for capturing data according to attributes described by entity from persistent mechanisms throughout life time of object in memory.

\subsubsection{Activation Bars}
 \begin{center}
 \includegraphics[width=320pt,height=200pt]{assets/image019.png}
 % image003.png: 0x0 px, 0dpi, nanxnan cm, bb=
\end{center}
Here the Participant1 object passes the message with arguments passed to participant2 so that participant2 will receive the data and sends it to the message caller provided by participant2. Message(argument) represents the method signature passed from an object to determine what message is passed, along with supplemental information to the client thus being useful.
\newline\newline
Exchanging information between objects along their lifelines, from which an object needs to be activated and de-activated, however when activated it does not perform an operation remains idle, however a bar is drawn to determine that the object is activated thus interacting throughout its life time.

\subsubsection{Nested Messages}
We can have nested messages being passed from object to another according to their bars, so we are limited to just one message containing a message call. Please note this is a sequence message, because flow starts from top left of participant to until participant 2 and 3, from which a message containing the output will be passed back from participant 3 to 2, then to participant 1. 
\newline\newline
Since synchronous message, there must be response which must be passed back to caller.
Synchronous and Asynchronous Message
Synchronous – Message is sent but does not need to wait for receiver, if an arrow with full arrow head.
\newline\newline
Asynchronous – Open arrow head does expect message to be sent and delivered back as it waits for further communications.
\newline\newline
Creation message -  A message for creating an object
\newline\newline
Destruction Message – Provides destruction message to destroy object and free from memory via the deconstructor.
\newline\newline
Deconstructors – special method in programming languages represented as ~Student() {} for housekeeping
 
\subsubsection{Alternative Fragment}
 
Alternative fragement shows a choice of behaviour for an object, only one of the options are executed as an alternative where one option being selected or the other.
Such as with a Humanoid robot selecting an object from either left, or right in a table, the control objects will be used so that the humanoid robot would reach and grab the object from the right hand if it was on the right side of the table, or by reaching the object from the leftHand, thus controlling the Humanoid robot from its left hand.
Here ALT is a guard used for controlling the object’s behaviour in terms of making decisions, and can invoke other operations if required.
\newline\newline
An optional fragment can be denoted using Opt, where the choice can be used or not according to the guard, such as the Boolean expression referring to the condition isReachable for the humanoid robot, if it can reach the object and that it is within range we expect whatever target to be reached, such as by reaching the object and eventually return a message.
Since this is asynchronous we may end up waiting for the arm to move and then grasp the object, imagine if the object was bounced, or if the robot has crashed, then it will cause an issue for the program. To avoid this, we can use synchronous arrows for the reach(T) message being passed so that robot can perform any other operation whilst grasping the object rather than waiting for a message to be passed.
\\\\
Guards are simply conditions controlling the behaviours of an object based on its decision points.

\subsubsection{Loop Fragment}
Is where an Boolean expression evaluated will be iterated over a number of times. 
\\\\
Sufficient for software developer to understand the logic done, such as we can expect that the operation can be finished until ten iterations, however we must provide sufficient information so that developers will understand the criteria from the loop.
Any other fragments can be nested inside the loop fragment.

\subsubsection{Reflexive calls} 
Reflexive calls allows messages to be transmitted to messages itself such as the EyeController being able to percept information, and create an object to see the visual perception such as if it sees a cup it generates a VisualObject to represent the visual perception of the cup. 
\\\\
From which the synchronous message will be sent back to the humanoid, which will send the fixate object to place object in the centre of the eye for an object to EyeController so that it can reach and grasp the object, and will use a reachStep reflexive call to move towards the objects to gradually build projectory in motors by gradually incrementing the movements of mechanism of arm via the arm controller. 
\\\\
Thus creating, a TactileObject of a haptic representation of the cup object, in which the message is then destroyed.

\subsubsection{Deriving Objects from Use cases}
Picking representative scenarios which led to the primary, alternative and exception paths.
Expressed in sequence diagrams to describe what happens in scenario from the natural language, based on the objects interactions participating the domain and system objects.
Rework sequence diagrams as sequence diagrams.
Combine communication diagrams to outline overview of class diagrams.

\subsection{Assume from scenario}
  
The following is a scenario which can be mapped into a sequence diagram.
First we would determine the actors and their lifelines as well as lifelines for boundary objects with enough space to draw.
And how the actor will interact with the system.
 
From the boundary object we would need to create another object, such as creating an entity object to persist data charactertised from customer entity, order details using entity objects, delivery details using entity objects to persist data according to their characteristics.
\\\\
For each boundary object we need to determine if there is a need to involve another object.
Then creating control objects for handling the logic of the data and to represent business decision-making, such as the sales operative requesting order to be created. This can be created by object and declaring its lifeline.
\\\\
Once we have analysed the scenario we would see this

Along with the diagram generated
 
Which maps the basic flow according to the use-cases for each object by using arrows, such as the sales operative dealing with the basic flow, then retrieves data from the entity object, from which the order is created by the Order Creator for handling the logic to make decision making, and then the delivery is stored as an output to deliver object.

\subsection{Communication Diagram}
 
Represents information similar to sequence diagrams, whatever is underline in boxes is an object, number is ordered to use-cases according to steps likewise in sequence diagrams.
 
For request creation of order it is 5.1 as it is part of step 5, once details have been processed by the sales operative from entity objects.
For creating an order it is, 5.1.1 because it is part of the sequence.
For creating the delivery it is 5.1.2 because it is part of creating an order.
This allows class diagrams to be easily created, by changing the notations slightly, be removing underline from boxes and expand boxes with relations, attributes, and multiplicities. This includes providing the services that are provided by the object such as createOrder() method provided by the OrderCreator classes.
 
The process of taking your designs from use-case and use-case description can be derived to sequence diagrams to determine interactions between objects, communicate diagrams when can be derived into class diagram.
\\\\
Models generated are aggregated to formulate a class model of the software system.
\\\\
However, we can use whichever UML diagrams preferred for communicating the designs and model according to methodologies, such as state machine diagrams which are often used in practices to model Actor and GUI interactions as part of boundary objects.
\end{document}
