with Ada.Text_IO; use Ada.Text_IO;

procedure num1 is
    type My_Int is range -1 .. 20; 
    -- restrict it from being less than -1 and greater than 20
    
    -- Gas reading generic type
    -- delta will result in some precision
    -- if high/low then results into poor precision
    type Gas_Reading is delta 0.01 range 0.0 .. 1.0;
    
    -- defined variable
    a1 : My_Int;
    gas : Gas_Reading;
    gas1 : Gas_Reading;

    -- actual procedure
    begin
        a1 := 19; --value assigned to variable
        
        --The following will restrict it to 0.1
        --due to being type float
        --gas := 0.11;

        -- same goes to here, if delta is low precision is low
        -- though if delta is high it is also low.
        gas := 0.75;
        gas1 := 0.03;

        -- if we attempt to exceed range will cause a run-time error
        -- though it will compile it
        -- gas := 1.1;

        --Outputs message to the terminal
        Put_Line ("Using the Generic Type My_Int"); 
        Put_Line ("Value of a1 is:" & My_Int'Image (a1));
        Put_Line ("The current gas reading is:" & Gas_Reading'Image (gas + gas1)); 
end num1;