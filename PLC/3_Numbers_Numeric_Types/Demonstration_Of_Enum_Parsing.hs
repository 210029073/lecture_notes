import Data.Maybe (isNothing)
data CAR = SPORT | ECONOMY | LUXURY | RACE
    deriving(Show, Read)

-- Before we can proceed with parsing, we need to make sure that we define a type, to differentiate between
type Mobile = CAR -- A defined type for the enum CAR

-- This is responsible for parsing the line to its element, though it uses printCarToString to perform the whole
-- process of parsing, essentially the dirty work.
printCar  = 
    case printCarToString line of
        Just element -> return element
        maybe -> printCar

-- This will be responsible for parsing the line of type String to its corresponding type
-- which is Mobile
printCarToString line = 
    do
        case reads line of -- Does the parsing character by character
            [] -> Nothing --If there is no interpretation that is a Mobile type, Nothing is then returned.
            ((n,_) : _) -> --there must be at least an interpretation of n in order to be classed as a Mobile type
                Just (n :: Mobile)



line = "SPORT" -- The actual string that will be parsed to the type of Mobile

main =
    do
        if isNothing printCar then
            putStrLn "The following car is: Invalid"
        else putStrLn $ "The following car is: " ++ show printCar --show is responsible for formatting the Mobile type back to a String
