#include <stdio.h>

#define NaN 0 //This defines a value as Not A Number to zero.

char character[] = "23.567";

int main() {
    char *txt = character;
    printf("Value is: %s", txt);
    int i = NaN; //with the value zero
    sscanf(txt, "%u", &i); //responsible for parsing string value
    printf("\nParsed value of i is %u", i);
}
