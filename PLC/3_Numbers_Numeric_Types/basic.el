;;ELISP file, different to CLISP but is executable in Emacs.
;; to execute simply run c-x c-e in order to evaluate an elisp expression
;; or load it as a current buffer in lisp interactive shell via emacs.

;; defining variables
;; lexical static scope
(setf a 2)

;; dynamic scope
(defvar b 2.0)

;;  basic arithmetic
;; N.B: Expressions are usually evaluated backwards in lisp.
(setq e (/ 5.00002 2.33))

(type-of e) ;this will check the type of the variable

; this will check if variable e is an integer type
(defun isInteger()
  (if (integerp e)
      (format "%f is an integer" e)
    (format "%f is not an integer" e)
    )
)

; similar to printf in c, which is used to concatenate the output
; according to format
(format "The value of n is %d" (* 2.5 5))

;; defining an enum type
;; since we cannot explicitly define an enum in lisp, one has to define a list.
;; CONSTANTS ARE REPRESENTED AS SYMBOLS
;; WHICH IS TRANSLATABLE TO ALL PROGRAMS usually denoted as 'AVALUE
(defun phonerangep(a)
  (member a `(MIDRANGE LOWRANGE HIGHRANGE))
)

(defun is-phone-rangep(element)
  (let*
      (
       (e1 (read-from-string element))
       )
    (if (phonerangep e1)
	(prin1 "\n")
	(format "\n\nTRUE")
    (format "\n\nFALSE"))))

(setf v "midrange1")
(is-phone-rangep "highrange") ; this will not work as it is of type cons
(is-phone-rangep 'highrange) ;this will work as it is a symbol

(defun run()
(princ "\ninput phone range")
(setf line (read))
(type-of line)
(is-phone-rangep line))
