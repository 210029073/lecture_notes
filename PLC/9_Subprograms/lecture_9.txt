What is Subprogram
It is often referred to as a program on behalf of itself, though it is a block that is part of a program that may be called from other program blocks.

What is Subprogram Abstraction?
Where a subprogram is capable of achieving abstraction via implementing a single high-level operation holding an expected outcome that is clear when executed.

N.B: It is said that a programmer should be able to tell what the outcome of the result is based on the name of the sub program.

How it achieves abstraction?
It essentially hides the low-level details, and highlights the key aspect of the system or activity that is a must to know in order to use the subprogram.

N.B: More or less of what it does rather than knowing how it does it. From which it is targeted to users rather than maintainers, or system application developers.

It should be made clear that:
What parameters are required to be taken by the subprogram. If so:

How many is needed and of what type?

Is there a pre-condition set to be satisfied before method call?

If there is a parameter that requires a pointer we need to provide in advance the referenced value that is to be modified during the subprogram execution not before?

Which of the parameters's value will be altered after the method invocation.

Does the subprogram require to have access an external variable, that is not part of the subprogram block? If so:
Does it involve reading them?

Does it involve modifying them?

Does the subprograms perform any I/O related operation from I/O devices?

Does the subprogram return a returning value after invoking its operation? If so:
How many values will be returned? Is it of a composite type or a single type.

How does the parameter values, or if any where included of external variables, values from devices relate to the actual return value?

There are various types of subprogram and it depends on whether if it is a procedure or function subprogram that will deliver a certain effect.

Though we can create pure procedure which achieves a certain goal though it does not return a value. Such is referred to as a void method.

With pure function they do return a single value though it depends on the what is available from the functional parameter.

Though with a PURE function it cannot alter any global variables, nor it can influence/alter an input/output device.

Do Functions and Procedures share similarity?
Although they do share similarities, they still have differences according to commands and expressions.

How do we execute a subprogram?
It is important to note that a single thread's lifetime is defined based on a single subprogram.

Java contains a main method that has a single thread in order to execute the subprogram.

Where in Ada, the main thread is executed from the top procedure defined based off the name of the file similar to Java.

Interpreter based languages
Though in Lisp, Python it is done differently, as the main thread is defined in a file where commands defined are executed sequentially, treated as if the whole file is a main program.
Since these are interpreter based languages calls are made possible once the thread has acknowledged the presence.

Compared to compiler-based languages as they are prepared before direct execution before main thread is executed to get a rough idea of the method calls.

What is a call stack?
A call stack is often referred to as a stack which keeps track of what subprogram needs to be executed according to the top of the stack and return to the current one when one finishes.

N.B: For the main method, it is returned once all subsequent method calls have been completed, and are forgotten.

For a recursive method call, each recursive call is pushed to the call stack, once it has reached its base case, it will pop the subsequent recursive calls from the stack.

What is parameter passing?
Where arguements are passed within a thread for each subsequent subprogram.

N.B: A parameter can be passed in either way which is either the calling context and subprogram definition, such that they are both calling context.

What is actual parameter?
An expression that is passed in a method call, can be also a variable referred outside that method call.

What is formal parameter?
Parameter names that are declared within a subprogram method definition and are only accessible to that method.

N.B: Though when a value is being passed to the method parameter upon a method call it is said to be a formal parameter.

How is actual and formal parameters linked to one and another?
There are two types of mechanisms to achieve such purpose:
One is copy parameter passing mechanism.
Where the other one is reference parameter passing mechanism.

Such parameters are said to be special variables that are local to the subprogram, though can be represented in a call statement.

What is a Copy-in parameter (Value):
A type of parameter where the parameter that is assigned is provided the value given by the actual parameter, which can be treated as any other variable, which can be read and updated, though the parameter's value is not updated outside the program.

Can be though as a sandbox.

What is a Copy-Out parameter (Result):
An inverse of copy-in parameter, instead of the parameter holding a value, it is not initialised. So when the program is executed it is automatically assigned to a value, though when the subprogram's execution is finished, the variable holds a final value containing the result of the formal parameter.

N.B: Copy-out is an inverse of copy-in since the copy-out parameter transfers the value at the opposite direction at the end compared to copy-in which is at the beginning.

What is copy-in-copy-out (Value-Result):
A combination of the two parameters, where the actual parameter is treated as a variable or entity that is depicted as a memory reference. Though formal parameter is initialised with a value, likewise with copy-out once the subprogram's call has been executed the variable is transferred with a final value similar to the foriegn parameter.


N.B: The problem with using in out in Ada together, is that the compiler will treat them as variable parameters even though it is supposed to be a reference passing mechanism, affecting the subprogram's reference semantics.

What is a reference parameter mechanism?
Where the formal parameter does not have its own memory, though it reference a part in memory for retrieving the value of the actual parameter, thus actual and formal parameters are aliasing. Note this is reference parameter passing mechanism.

Why is reference parameter mechanism better?
No need to copy any values or store values in memory. Such is often a perfect choice when dealing with composite values such as records, arrays.

Problems that may be caused by reference parameter mechanism?
Risk of unintended behaviour when working with a value that is being aliased in two context.

How to solve such conflicts?
Forbid any changes to be made by the parameter by setting the parameter as a constant parameter. Though parameters that are not restricted to constant is referred to variable parameters which emphasises a volatile effect.

It is possible in languages such as C++, in terms of passing pointers as reference parameters, or copy-in mechanism by passing normal parameters.

//reference parameter passing in C++
int swap(int &x, int &y) {
    //details omitted.
}

//variable parameters in C++
int swap(int x, int y) {
    //details omitted
}

N.B: There is no support reference passing in C though can be simulated using pointers.

How does Java do parameter passing?
Java features a builtin copy-in parameter mechanism, due to referencing semantics of variable types, likewise in languages such as C.

N.B: In Ada, in and out keywords for accompanying either copy-in, or copy-out or both parameters as in out.

Such as
procedure calculateOutstandingTax
    (
        resultingTax : in Integer
        remaining: out Integer
        deductedTaxFromBalance: in out Integer
    )

is
being
    -- Code Omitted.
end calculateOutstandingTax;

Some definitions
================
What is parameter aliasing?
Where a method's parameter contains two or more variable referring to the same value in memory, the problem with this is that with reference passing it causes some unintended side effects within the program.

What is meant by referential transparency?
Replacing a function's code by an expression, or a variables value without affecting the behaviour, or the output of the program.

What is meant by a procedure?
A procedure is a special type of subprogram that is responsible of modifying the values of external varaible that it has access to, and values that it has collected from devices.

What is meant by a function?
A function is a subprogram that is responsible creating and returning either a composite or single value.

What is meant by an expression?
Expression often refers to anything that can be evaluated to produce a value, though it cannot modify a program's state.

What is meant by a command?
Commands generally modify the value of a given program's state, which is its variables throughout the lifetime of the program, though it does not create a value.

What is copy parameter passing mechanism?
Where the parameter passed in a method call is a local varaible which starts during the lifetime of the subprogram and seizes to exist at the end.

The actual parameter is copied from the start of the call to the formal parameter, sometimes when the call has finished, the value of the formal parameter is then passed to the actual Parameter.

What is reference parameter passing mechanism?
Where the actual parameter is a single variable, which is said to be a formal parameter variable during the call, though the parameters' value is stored, there is no copying the values, as the value of that parameter is altered or accessed directly.
