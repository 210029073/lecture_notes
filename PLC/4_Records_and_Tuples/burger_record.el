;; Defining types of burger records in lisp (Prepared by Ibrahim Ahmad)
;; Requires Emacs in order to be executed.
;; Run Ctrl-X Ctrl-E to Execute line of code.

;; This is a function that will evaluate the burger record via associative list
;; Surrounded by and or brackets

;; Must be a list of type listp
;; burger must be either length of 2 or 3.

;; If of length 2, then it will evaluate the first variant and associate the
;; values with the following records.
;; will check if there is two of those parameters

;; Otherwise second variant of record with three components will be evaluated
;; it will check if there is three of those parameters
;; otherwise nil will be returned if there is no match.

(defun burgerp (burger)
  (and (listp burger)
       (or (and (= 2 (length burger))
		(let ((name (cdr (assoc 'name burger)))
		      (dollar (cdr (assoc 'dollar burger))))
		  (and name dollar)))
	   (and (= 3 (length burger))
		(let ((name (cdr (assoc 'name burger)))
		      (chiptype (cdr (assoc 'chiptype burger)))
		      (euros (cdr (assoc 'euros burger))))
		  (and name chiptype euros))))))

;; responsible for printing the value automatically to string
;; thus does the formatting
(defun burger-to-string(burger)
  (and (burgerp burger)
         (if (= 3 (length burger))
	   (format "Euro Style Burger:\nBurger name -> %s, Chip-Type -> %s, Burger Price -> €%f" (alist-get 'name burger) (alist-get 'chiptype burger) (alist-get 'euros burger))
	   (format "American Style Burger\nBurger name -> %s, Burger Price -> $%f" (alist-get 'name burger) (alist-get 'dollar burger)))))

;; Defining the associative list for the following variants defined from
;; the above.
(defvar americano '((name . "americano") (dollar . 0.94)))
(defvar french '((name . "french royal burger") (chiptype . "french fries") (euros . 1.79)))

;; This is a type of american burger, which will be evaluated with
;; two parameters
(burger-to-string americano)

;; this is a type of french burger, which will be evaluated with
;; three parameters
(burger-to-string french)
