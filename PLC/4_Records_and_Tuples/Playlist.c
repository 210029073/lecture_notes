#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct Person {
	char firstName[10];
	char lastName[10];
	int age;
} Person;

typedef enum ItemType {
	MOVIE, PIECE
} ItemType;

typedef struct Movie {
	Person person;
	char name[10];
	int year;
	int popularity;
	char* isEmpty;
} Movie;

typedef struct Piece {
	Person person;
	int year;
	float duration;
	int popularity;
	char* isEmpty;
} Piece;

typedef union ItemVariant {
	struct Movie movie;
	struct Piece piece;
} ItemVariant;

typedef struct Item {
	ItemType type;
	ItemVariant item;
} Item;

typedef enum Errors {
	ITEM_NOT_FOUND
} Errors;

typedef enum Boolean {
	true, false
} Boolean;

static const char * const boolean_type[] = {
	[true] = "true",
	[false] = "false"
};

typedef struct Data {
    int size;
    char content[255];
} Data;

Movie movie = { {"John", "Doe", 64}, "Movie", 2005, 25.0 };

Piece piece;

struct Data toString(Item* item) {
    Data output;
    output.size = 255;
    output.content[output.size];
    char result[100];
	if (item->type == MOVIE) {
        strcpy(result, item->item.movie.person.firstName);
        strcat(result, " ");
        strcat(result, item->item.movie.person.lastName);
        strcat(result, " - ");
        strcat(result, item->item.movie.name);
        strcat(result, " (");
        char mYear[20];
        int mvYear = item->item.movie.year;
        sprintf(mYear, "%i", mvYear);
        strcat(result, mYear);
        strcat(result, ")");
	    strcpy(output.content, result);
		return output;
	}

	else if (item->type == PIECE) {
        strcpy(result, item->item.piece.person.firstName);
        strcat(result, " ");
        strcat(result, item->item.piece.person.lastName);
        strcat(result, " - Duration ");
        char pieceDuration[sizeof(item->item.piece.duration)];
        float duration = item->item.piece.duration;
        sprintf(pieceDuration, "%0.2fs", duration);
        strcat(result, pieceDuration);
	    strcpy(output.content, result);
		return output;
	}

	return output;
}

char* isThereMovie(Item* item) {
	if(item->type == MOVIE && item->item.movie.isEmpty == "false") {
		return boolean_type[0];
	}

	return boolean_type[1];
}

int isTherePiece(Item* item) {
	if(item->type == PIECE && item->item.piece.isEmpty == "false") {
		return boolean_type[0];
	}

	return boolean_type[1];
}

char* isItemComplete(Item* item) {
	return isThereMovie(item) == "true" && isTherePiece(item) == "true" ? boolean_type[0] : boolean_type[1];
}

void start() {
    	Person p;
	p.firstName;
	strcpy(p.firstName, "Roald");
	p.lastName;
	strcpy(p.lastName, "Dahl");
	p.age = 64;

	Movie movie;
	movie.name;
	strcpy(movie.name, "Matilda");
	movie.person = p;
	movie.year = 1992;
	movie.popularity = 26;
	movie.isEmpty = "false";

	Item item;
	item.type = MOVIE;
	item.item.movie = movie;
	Data output = toString(&item);
	printf("Movie ->\n");
	printf("%s", output.content);
	
	printf("\n\n");
	
	Piece piece;
	
	Person pPiece;
	pPiece.firstName;
	strcpy(pPiece.firstName, "John");
	pPiece.lastName;
	strcpy(pPiece.lastName, "Doe");
	pPiece.age = 64;
	
	piece.year = 2016;
	piece.duration = 1 * 60 * 0.33 * 60;
	piece.person = pPiece;
	piece.popularity= 10;

	item.type = PIECE;
	item.item.piece = piece;
	item.item.piece.isEmpty = "false";
	output = toString(&item);
	printf("Piece ->\n");
	printf("%s", output.content);

	printf("\n\nIs there a Movie? ->");
	printf(" %s\n", isThereMovie(&item));

	printf("\nIs there a Piece? ->");
	printf(" %s\n", isTherePiece(&item));

	printf("\nIs item complete? ->");
	printf(" %s\n", isItemComplete(&item));
}

int main() {
    start();
	return 0;
}
