\documentclass[a4paper, 11pt]{report}
\usepackage{geometry}
\usepackage[pdftex,pdftitle={Abstract Data Types in Java}, pdfauthor={Ibrahim Ahmad}, pdfsubject={Data Structure and Algorithms}, pdfkeywords={ADTs, Operations, Map, Set, Lists, Queues, Linear, Non-linear}]{hyperref}
\geometry{a4paper, margin=2.54cm}

\usepackage{listings}
\usepackage{bookmark}
\usepackage{graphicx}

\usepackage{nimbusmono}

\usepackage{fancyhdr}
\pagestyle{fancy}
\usepackage{listings}
\usepackage[T1]{fontenc}
\usepackage[many]{tcolorbox}

\title{Abstract Data types and Collections}
\author{Ibrahim Ahmad}

% Use this for styling using the lstlisting library 
% language from language property to any language 
% \begin{lstlisting}[language=langauge, numbers=left,  numberstyle=\footnotesize, keywordstyle=\color{blue},   commentstyle=\color{gray}, basicstyle=\small\ttfamily]
	
	%\end{lstlisting}

\begin{document}
\chapter{Abstract Data Type}
\section{Abstract Data Type}
An abstract data type is simply an \textbf{\textit{abstract structure}} that \textbf{\textit{holds}} a set of \textbf{\textit{data}} and a set of \textbf{\textit{operations}} performed on the actual data \textbf{\textit{stored}} within a collection. Such data type is referred to as \textbf{\textit{abstract}} as its focus is on the \textbf{\textit{definition}} of \textbf{properties} from the set of data, and the \textbf{behaviour} of operation without any implementation issues.\\\\
\subsection{ADTs Don't affect the use of data types} However, \textbf{\textit{non}} of the operations \textbf{\textit{defined}} within an abstract data type act upon the data, and they do not affect the use of data types. Although the implementation details of the abstract data type may still change, however it will \textbf{\textit{not}} involve the client to be aware of the implementation of the abstract data type, only which is appropriate for the job.
\subsection{Determining the logical structure of ADT}
With an ADT we are able to determine the logical structure of the data type, regarding how they are organised, either \textbf{\textit{linearly}}, \textbf{\textit{hierarchically}}, or if they \textbf{\textit{ordered}}, or \textbf{\textit{unordered}}. However, we are not required to know who the data type is actually implemented, just \textbf{\textit{storing}} them in the appropriate abstract structure. 

An \textbf{\textit{abstract structure}} would be a list, queue, set, map and tree, a rational number, fraction etc.
\\\\
The same ADT can be \textbf{\textit{implemented}} in many ways, such as a stack can be \textbf{\textit{created}} using an Array or a linear linked structure \textbf{\textit{using}} a pointer to point to the next element. Stacks follow a \textbf{\textit{last-in-first-out}} routine. In other words, elements that are \textbf{\textit{stored}} within a stack is added to the \textbf{\textit{top}}, and is removed from the \textbf{\textit{top}}, and can \textbf{only} be removed one-by-one rather than randomly, and \textbf{\textit{peak}} the top element in a stack. Does not need to be iterated, so there is no need to implement Iterable. However, when you insert another element within a stack the \textbf{\textit{previous}} element will be \textbf{\textit{moved}} downwards.
\\
A \textbf{\textit{typical operation}} focuses on what is \textbf{\textit{performed}} on the data.
\begin{figure}[h]
	\centering
	\includegraphics[width=0.7\linewidth]{abstract_struct}
	\caption{Examples of ADT}
	\label{fig:abstractstruct}
\end{figure}

\section{Collections}
A collection is simply referred to as an object which holds a group of objects based on some fixed type. Depending on the type of a collection, the \textbf{\textit{group of objects}} stored within a collection can be organised in many ways such as with an index, or based on relation with a key resulting \textbf{\textit{key-valued pairs}}.

\subsection{Organisation of Collections}
Collections can be organised in many ways:
\begin{enumerate}
	\item \textbf{Unordered} - elements will be stored without any order such as a set, multiset.
	\item \textbf{Linear} - elements will be sorted based on a given position such as an array, list, stack, queues etc.
	\item \textbf{Non-linear} - stores element in a hierarchical manner such as establishing relation between the elements such as a tree, or graph like structure for complex data types. 
\end{enumerate}
\begin{figure}[h]
	\centering
	\includegraphics[width=0.6\linewidth]{collection_types}
	\caption{Represents how collections can be categorised.}
	\label{fig:collectiontypes}
\end{figure}
In general elements are ordered based on how they are \textbf{\textit{added}} to a collection, or if there some \textbf{\textit{relationship}} between the elements.
\begin{figure}[h]
	\centering
	\includegraphics[width=0.7\linewidth]{relationship_elements}
	\caption{Represents the relationship between elements such as the person's name, and their year.}
	\label{fig:relationshipelements}
\end{figure}
The idea is that regardless of what collection we will encounter we must choose which one according to our objectives, and the \textbf{\textit{likely}} usage of that collection whether if the requirement is for allowing \textbf{\textit{faster}} data access, or \textbf{\textit{better}} insertion.
\\\\
However, the internal structure of a collection is not required to be similar to its logical structure. For instance, a list can either be \textbf{\textit{linear ordered}}, or \textbf{\textit{indexed-based list}}, or a list can be \textbf{\textit{non-linear}} such as being stored internally in a binary search tree like structure which provides $O(n \log n)$ searching performance.
\subsection{Bounded and Unbounded Collections}
In Collections, we are able to \textbf{\textit{set}} a limit to capacity, or add an \textbf{\textit{unlimited}} number of elements to the collection, and we differentiate them by referring to them as \textbf{\textit{bounded}} and \textbf{\textit{unbounded}} abstract-data-types.

\begin{enumerate}
	\item \textbf{Bounded} - The collection has a \textbf{\textit{maximum}} size, or a \textbf{\textit{limit}} to capacity.
	
	\item \textbf{Unbounded} - There is \textbf{\textit{no limit}} to the size, or collection being stored.
\end{enumerate}
	A typical bounded collection that would most likely be encountered with is an array, when an array is \textbf{\textit{created}} we \textbf{\textit{must}} know beforehand the capacity that we wish to store the elements. Any collection that is considered \textbf{\textit{bounded}} would be an \textbf{array}.
	\\\\
	The ArrayList provided by the {\ttfamily{java.util.ArrayList}} is considered to be \textbf{\textit{unbounded}} as it acts as an array that can be re-sized. Once the ArrayList exceeds its initial capacity it is then copied to an array with \textbf{\textit{twice}} of its capacity.
	However, the ArrayList \textbf{\textit{can}} be bounded by a capacity size during initialisation, but it will expand when \textbf{\textit{adding}} more elements.

\section{Requirements for an ADT}
For an ADT, we would expect that an ADT would be able to:
\begin{enumerate}
	\item Enable the client to \textbf{\textit{determine}} if the \textbf{\textit{collection}} is empty or not via an \textbf{\textit{isEmpty()}} method.
	
	\item Determine the number of elements that are \textbf{\textit{stored}} within a collection via a \textbf{\textit{size()}} method. 
	
	\item Remove an element at a \textbf{\textit{certain}} way, depending on how the collection is implemented via a \textbf{\textit{remove()}} method.
	
	\item Insert an element to a given collection depending on how it is implemented.
\end{enumerate}

For a Bounded ADT, we would expect it to:
\begin{enumerate}
	\item Check if permissible that a collection is \textbf{\textit{full}}.
	
	\item Determine the \textbf{\textit{maximum}} possible size of the collection.
	
	\item Check if the collection's capacity has been \textbf{\textit{exceeded}} by either \textbf{\textit{exceptional handling}}, which involves \textbf{\textit{throwing}} an exception if the client has exceeded the capacity using the {\ttfamily{IllegalStateException}}.
\end{enumerate}

\subsection{Remarks - Removing and Insertion Operations}
As intended, we \textbf{\textit{must}} make sure that we cannot remove element in a collection that is \textbf{\textit{empty}}, the \textbf{remove()} method should automatically throw an exception such as an {\ttfamily{IllegalStateException}} which is surrounded by a \textbf{\textit{try}} and \textbf{\textit{catch}} block, or we could instead use the \textbf{\textit{isEmpty()}} to check if the collection is empty before attempting the removal operation.
\\\\
The same \textbf{applies} for performing \textbf{\textit{insertion}} over a \textbf{\textit{bounded}} collection as we must not exceed the capacity, either \textbf{\textit{throw}} an {\ttfamily{IllegalStateException}} if capacity becomes full, or by using a convenient \textbf{\textit{isFull()}} method.

\section{Collections and Abstractions}
Abstraction \textbf{hides} the \textit{\textbf{unnecessary}} implementation details, which is useful for \textbf{\textit{managing}} complexity of a large system. For instance, with an ADT like a HashMap we only need to provide the \textbf{\textit{typical operation}} rather than the \textbf{\textit{implementation}} details. \textbf{\textit{What}} it does, rather than \textbf{\textit{how}} it works.
\subsection{A Way for Abstraction - Java Interfaces}
In Java, we can \textbf{\textit{achieve}} abstraction by using \textbf{\textit{Interfaces}}, as it enables programmers that \textbf{\textit{develop}} applications to \textbf{\textit{focus}} on the \textbf{\textit{typical}} operations that an object \textbf{\textit{should}} do, rather than how it works.
\subsection{Iterator Interface - An Example}
For example, the Iterator interface from the {\ttfamily{java.util.Iterator}} \textbf{\textit{achieves}} abstraction by mainly \textbf{\textit{defining}} methods that are \textbf{\textit{abstract}} such as {\ttfamily{hasNext()}}, and {\ttfamily{next()}}. When the application programmer \textbf{\textit{uses}} the Iterator, they will either use it for \textbf{\textit{iterating}} over elements within an Array, or a Linked-List, it does \textbf{\textit{not cause}} any concerns for the \textbf{\textit{application}} programmer. 
\subsection{The Flexibility of Interfaces}
With Interfaces it \textbf{\textit{provides}} the flexibility of being able to \textbf{\textit{implement}} an {\ttfamily{Iterator}} for a specific purpose such as an {\ttfamily{ArrayIterator}}, or a {\ttfamily{LinkedIterator}} being assigned to a variable, and point to as an {\ttfamily{Iterator}} object.

\subsection{Collections in General}
For any object defined, a Collection is an abstraction, as we want to ensure that we establish a client-server model between client so that the client will be able to access the service provided by the collection, while the implementation details remain hidden from the client so that the client will only need to know how the collection provides its service.
\begin{figure}[h]
	\centering
	\includegraphics[width=0.6\linewidth]{interface_abstraction}
	\caption{An interface abstracts the implementations of a collection.}
	\label{fig:interfaceabstraction}
\end{figure}

The separation \textbf{\textit{provides}} many benefits, after implementing a system we can \textbf{\textit{alter}} the implementation details of a collection \textbf{\textit{without}} even affecting \textbf{\textit{any}} classes that represents the system.
\pagebreak
\subsection{Remarks}
To Summarise about collections:
\begin{enumerate}
	\item \textbf{Abstract Data Types} - Used to \textbf{\textit{hold}} multiple datasets and \textbf{\textit{carry}} operations on the data.
	\item  \textbf{Data structures} - Used to \textbf{\textit{implement}} data types \textbf{\textit{from}} programming constructs.
\end{enumerate}
Please note that all methods \textbf{\textit{defined}} in an interface are \textbf{\textit{set}} to the visibility \textbf{\textit{public}} and \textbf{\textit{abstract}} by \textbf{default}, so you \textbf{\textit{do not}} need to explicitly defined {\ttfamily{public abstract}}. \\\\Moreover, for any method that implements a List-like interface, we do not need to include the abstract method {\ttfamily{iterator()}} as once you \textbf{\textit{extend}} {\ttfamily{Iterable}} in the custom List interface, {\ttfamily{Iterable}} has already \textbf{\textit{included}} the method {\ttfamily{iterator()}}.

\section{Issues with Collections}
Though Collections are a useful utility in Java software development, they are used \textbf{\textit{most}} often in applications for working with large datasets that have homogenous (same) types of objects. Understanding the \textbf{\textit{characteristics}} would allow us to make better decisions for which collection to use. However we \textbf{\textit{must}} consider to following:

\begin{enumerate}
	\item Understanding \textbf{\textit{how}} the collection works.
	\item The problems that the collection will \textbf{\textit{solve}}.
	\item Steps to \textbf{\textit{define}} the interface.
	\item How we might \textbf{\textit{implement}} it?
	\item Cost and benefits in terms of \textbf{\textit{storage}} usage, and execution \textbf{\textit{time}} for each implementation.
\end{enumerate}

\section[Interfaces and ADTs]{Java Interfaces and Abstract Data types}
Java, provides a convenient way of \textbf{\textit{creating}} abstract data types and that is by using \textbf{\textit{Interfaces}} as it can \textbf{\textit{achieve}} abstraction. Interfaces \textbf{\textit{provide}} formal declaration of \textbf{\textit{typical}} operations that a one or more classes will provide. Since Interfaces \textbf{\textit{restricts}} data member from being represented, it \textbf{\textit{only}} provides an abstract view of its operations \textbf{\textit{provided}} by a collection along with \textbf{\textit{generic}} types being used for collection data structures.
Since Interfaces \textbf{\textit{cannot}} be used for \textbf{\textit{concrete}} implementations of data structure, classes \textbf{\textit{implement}} the data structure, and the operations of the interface.
\\\\
It should make sense that the operations \textbf{\textit{acting}} on a collection would be heavily \textbf{\textit{dependent}} on the nature of those objects. For example, \textbf{\textit{adding}} a book to a library can be referred to as a \textbf{\textit{collection}} of books, which is similar to \textbf{\textit{adding}} a football team to a football league such as a \textbf{\textit{collection}} of leagues, regardless of what type of entity we are \textbf{\textit{working}} with they \textbf{\textit{still}} an object whether they are a book etc. Similarly, we can \textbf{\textit{count}} a collection of books to \textbf{\textit{determine}} the number of books in the library.
\section{Stacks}
\begin{figure}[h]
	\centering
	\includegraphics[width=0.4\linewidth]{stack}
	\caption{Stacks are represented vertically.}
	\label{fig:stack}
\end{figure}

Stacks are \textbf{\textit{linear}} collections which can only insert and remove elements from one end which is the top. You can think of stacks like \textbf{\textit{boxes}} stacked on top.
Stacks follow a \textbf{\textit{Last-In-Last-Out}} routine, meaning the last element is \textbf{\textit{placed}} on top, causing \textbf{\textit{all}} previous elements to \textbf{\textit{move}} downwards, and the \textbf{\textit{last}} element that was inserted is usually \textbf{\textit{removed}} first from the top. They can be represented as arrays.
\subsection{Operations of a Stack}
\begin{figure}[h]
	\centering
	\includegraphics[width=0.3\linewidth]{stack_interface}
	\caption{Represents a Stack interface.}
	\label{fig:stackinterface}
\end{figure}

The following operations that are defined within a stack are:
\begin{enumerate}
	\item Check if the stack is \textbf{\textit{empty}} through {\ttfamily{isEmpty()}}.
	\item Determine the \textbf{\textit{size}} of the stack through {\ttfamily{size()}}.
	\item \textbf{\textit{Add}} an element on \textbf{\textit{top}} of the stack using {\ttfamily{push()}}.
	\item \textbf{\textit{Remove}} an element on \textbf{\textit{top}} of the stack using {\ttfamily{pop()}}.
	\item See \textbf{\textit{only}} the element from \textbf{\textit{top}} of the stack using {\ttfamily{peek()}}.
\end{enumerate}

\subsection{Stack structure}
\begin{lstlisting}[language=Java, numbers=left,  numberstyle=\footnotesize, keywordstyle=\color{blue},   commentstyle=\color{gray}, basicstyle=\small\ttfamily]
public interface StackADT<T> {
	public void push(T element);
	
	public T pop();
	
	public T peek();
	
	public boolean isEmpty();
	
	public int size();
}
\end{lstlisting}

For a bounded stack we would create an interface which will extend {\ttfamily{Stack}} to check if the Stack \textbf{\textit{is}} full, and the capacity:
\begin{lstlisting}[language=Java, numbers=left,  numberstyle=\footnotesize, keywordstyle=\color{blue},   commentstyle=\color{gray}, basicstyle=\small\ttfamily]
public interface BoundedStackADT<T> extends Stack<ADT> {
	public boolean isFull();
	
	public int capacity();
}
\end{lstlisting}

Since we want to ensure flexibility in code reuse, we would simply create an interface called \textbf{\textit{Bounded}} to check both the \textbf{\textit{capacity}} and if a collection is \textbf{\textit{full}} rather than just for each version.

\begin{lstlisting}[language=Java, numbers=left,  numberstyle=\footnotesize, keywordstyle=\color{blue},   commentstyle=\color{gray}, basicstyle=\small\ttfamily]
	
//For setting fixed size stack.
public interface Bounded {
	public int capacity();
	
	public boolean isFull();
}

//This will inherit both the stack and bounded.
interface BoundedStackADT<T> extends Bounded, Stack<ADT> {}
\end{lstlisting}

\subsection{Remarks}
The {\ttfamily{toString()}} method defined within a Stack interface should be removed, since all classes that are defined inherit the {\ttfamily{toString()}} method from the \textbf{\textit{Object}} class. Although it is not a typical ADT operation, it should \textbf{\textit{not}} be included, as it will allow the {\ttfamily{toString()}} to reveal elements within a stack.

\section{Queue}
\begin{figure}[h]
	\centering
	\includegraphics[width=0.5\linewidth]{queue}
	\caption{A queue can be inserted horizontally at \textbf{\textit{both}} ends.}
	\label{fig:queue}
\end{figure}

A queue is a \textbf{\textit{linear}} data structure which has an \textbf{\textit{opening}} at each end. The elements are usually inserted at the \textbf{\textit{rear}} of the queue, and are removed from the \textbf{\textit{front}} of the queue. The queue itself is processed in a \textbf{\textit{FIFO}} routine, meaning elements \textbf{\textit{are}} removed in the \textbf{\textit{same}} order they were added.
Queues can be represented in real-life situations such as:
\begin{enumerate}
	\item A queue in the \textbf{\textit{supermarket}}.
	\item Service queue within a \textbf{\textit{post}} \textbf{\textit{office}}, or \textbf{bank}.
	\item Cars \textbf{\textit{waiting}} in a queue at a \textbf{\textit{traffic}} \textbf{\textit{light}}.
	\item Assembly line in a \textbf{\textit{factory}}.
\end{enumerate}
The main difference between queues and a stack are that, a queue is conceptually \textbf{\textit{represented}} horizontally, where stack is vertically, and that queue can \textbf{\textit{perform}} insertion and updating operation at \textbf{\textit{both}} ends, where a stack can \textbf{\textit{only}} do at \textbf{\textit{one}} end.
\\\\
Similar to a stack, a pure queue \textbf{\textit{does not}} permit any access or insertion at the \textbf{\textit{middle}} of the queue, \textbf{\textit{only}} from the opening ends.

\subsection{Queue operation}
\begin{figure}[h]
	\centering
	\includegraphics[width=0.3\linewidth]{queue_operations}
	\caption{Operations defined in a queue.}
	\label{fig:queueoperations}
\end{figure}

The operations that are defined in a queue are:
\begin{enumerate}
	\item \textbf{Enqueue} - Inserts elements at the \textbf{\textit{rear}} of the queue.
	
	\item  \textbf{Dequeue} - Likewise to \textbf{\textit{enqueue}}, \textbf{\textit{dequeue}} is \textbf{\textit{capable}} of removing elements \textbf{\textit{head}} of the queue.
	
	\item \textbf{First} - Returns the \textbf{\textit{first}} element at the queue, \textbf{\textit{without}} removing it.
\end{enumerate}
We also have the operations {\ttfamily{isEmpty()}}, and {\ttfamily{size}}, which is \textbf{\textit{expected}} in any ADT.

\subsection{Queue structure}
We would also consider creating a {\ttfamily{Bounded}} interface for creating a bounded queue.
\begin{lstlisting}[language=Java, numbers=left,  numberstyle=\footnotesize, keywordstyle=\color{blue},   commentstyle=\color{gray}, basicstyle=\small\ttfamily]
public interface StackADT<T> {
	public void enqueue(T element);
	
	public T dequeue();
	
	public T first();
	
	public boolean isEmpty();
	
	public int size();
}
\end{lstlisting}

\section{Lists} 
\begin{figure}[h]
	\centering
	\includegraphics[width=0.5\linewidth]{list_structure}
	\caption{Lists can be inserted at any position.}
	\label{fig:liststructure}
\end{figure}

Similar to Stacks and Queues a List is also a \textbf{\textit{linear}} structure, but provides \textbf{\textit{flexibility}}. Typically, elements can \textbf{\textit{inserted}} to a list \textbf{\textit{anywhere}}. Lists can be implemented in many different ways, such as elements can be \textbf{\textit{inserted}} at a \textbf{\textit{certain}} position within the list.

\subsection{Differences between Queue and Stack}
The difference between a queue and a stack is that a list is capable of \textbf{\textit{searching}} through elements at \textbf{\textit{any}} position, where with a queue and stack it is \textbf{\textit{not}} possible, as you can \textbf{\textit{only}} look up at \textbf{one} end.

\subsection{List operations}
Though with other majority ADTs the insertion is usually the \textbf{\textit{same}}, however with list they are \textbf{\textit{dependent}} on the \textbf{\textit{type}} of list. So we do \textbf{\textit{not}} need to include the \textbf{\textit{add}} method to the interface.
\\\\
The operations which are defined within a list are:
\begin{enumerate}
	\item \textbf{removeFirst} - We are able to \textbf{\textit{remove}} the \textbf{\textit{first}} element.
	
	\item \textbf{removeLast} - We are able to \textbf{\textit{remove}} the \textbf{\textit{last}} element.
	
	\item \textbf{remove} - We are able to \textbf{\textit{remove}} an element from \textbf{any} position.
	
	\item \textbf{First} - Returns the element from the \textbf{\textit{front}} of the list.
	
	\item \textbf{Last} - Returns the element from the \textbf{\textit{rear}} of the list.
\end{enumerate}

\subsection{List structure}
\begin{lstlisting}[language=Java, numbers=left,  numberstyle=\footnotesize, keywordstyle=\color{blue},   commentstyle=\color{gray}, basicstyle=\small\ttfamily]
public interface ListADT<T> extends Iterable<T> {
	public T removeFirst();
	
	public T removeLast();
	
	public boolean remove(T element);
	
	public T first();
	
	public T last();
	
	public boolean isEmpty();
	
	public int size();
}
\end{lstlisting}
Although, the ADTs discussed earlier cannot iterate because they can \textbf{\textit{only}} \textbf{peek} through the element at \textbf{one} end, with lists we are able to \textbf{\textit{iterate}} over \textbf{\textit{all}} elements \textbf{\textit{defined}} in the list which involves extending {\ttfamily{Iterable<T>}}, since we are \textbf{\textit{implementing}} the {\ttfamily{Iterable}} interface we do \textbf{\textit{not}} need to include the {\ttfamily{iterator()}} abstract method.

\section{Set Collections}
In Computer Science and Mathematics a \textbf{\textit{set}} can be considered a collection containing \textbf{\textit{unique}} objects of a certain type, which are \textbf{\textit{unordered}}.
\\\\
In Mathematics, sets are \textbf{\textit{represented}} using \textbf{\textit{curly-braces}} \textbf{Vowels = {a, e, i, o,u}}. The order of the elements is \textbf{\textit{not}} a concern, so they are \textbf{\textit{impermeable}}. A set can only have \textbf{\textit{one}} in a set, so if we include another \textbf{a} within the Voewls set, the set will be \textbf{\textit{unchanged}}, so it will result the \textbf{\textit{set}} to have the \textbf{\textit{original}} five which is \textbf{Vowels = {a, e, i, o, u}}.

\subsection{Set operations}
\begin{enumerate}
	\item \textbf{Add} - We are able to add an element to the set.
	\item \textbf{Add all} - We can add multiple elements to the set.
	\item \textbf{Remove} - We can remove an element from the set, or all elements from the set.
	\item \textbf{Contains} - We can \textbf{\textit{check}} if an \textbf{\textit{element}} is a member of a set. Such as if $a \in A$
	\item \textbf{Retain} -  We can \textbf{\textit{retain}} all elements that are a \textbf{\textit{member}} of a given set.
	\item \textbf{Union} - Add \textbf{\textit{all}} elements from both sets into a new set.
	\item \textbf{Intersect} - Adds elements that are \textbf{\textit{part}} of \textbf{\textit{given}} set, and \textbf{\textit{another}} set to a new set. Such as $B \cap A$.
	\item \textbf{Subset} - Check if a \textbf{\textit{given}} set is a \textbf{\textit{subset}} of a \textbf{\textit{target}} set, meaning \textbf{\textit{all}} members are a \textbf{\textit{member}} of the target set. Such as if all elements of A are a member of B $B \in A$.
	\item \textbf{Difference} - This will \textbf{return} the elements of a given set, that is \textbf{not} a member of a target set.
\end{enumerate}

\subsection{Bounded Sets}
For bounded set operations we would return a random element from the set from the {\ttfamily{pick()}} method, and delete from a random element using {\ttfamily{removeRandom()}} method.

\subsection{Remarks}
There will also be both operations that will check if the set is \textbf{\textit{empty}}, and the \textbf{\textit{size}} of the set. We may also include the {\ttfamily{equals()}} method for sets equality, for determining if \textbf{\textit{both}} sets are \textbf{\textit{equal}} to each other.\\\\
We would use the equals method of class T, to decide if the element is present or not, when implementing the method {\ttfamily{contains()}}, rather than using equals.

\section{Multi-Set Collections}
A multi-set is capable of holding an \textbf{unordered} collection of elements of a certain type with \textbf{multiple} occurrences, where a set can \textbf{only} hold one instance of the same element where the order \textbf{\textit{does not}} matter.
For instance, a set that contains multiple elements such as two a's like ${a, a, e, i}$ is referred to as a multi-set, note a multi-set of ${a,e,i}$ is \textbf{different} to ${a, a, e, i}$. It is possible another similar element of a's into a set. Such as with a purse, we can as many one pound coins, or as many one pence coins.

\subsection{Multi-Set Operations}
A multi-set holds the \textbf{\textit{same}} methods \textbf{\textit{provided}} by the set method such as the {\ttfamily{\color{blue}isEmpty, size, contains}} methods.
\\\\
We can also have set \textbf{\textit{union}}, \textbf{\textit{intersection}}, and \textbf{\textit{difference}} operations within a multi-set.
\subsection{Occurrence of an element in both multi-sets}
\begin{itemize}
	\item Union operations occur \textbf{\textit{max(n,m)}} times in $A \cup B$ If one multi-set has two of each of green and blue LEGO blocks, and the other has 6 of each orange, brown LEGO blocks and we compare them, it is considered UNION.
	\item Intersection operation occur \textbf{\textit{min(n,m)}} times in $A \cap B$. If one multi-set has 3x green, 8x blue LEGO blocks, and the other has 1x red, 4x blue LEGO blocks it is considered intersection. It is still considered intersection true if one is also empty.
	\item Difference operations occurs \textbf{\textit{max(0, (n - m))}} times in $A / B$. If one multi-set has 6x red, 4x blue LEGO blocks, and the other has 5x green, they are referred to as \textbf{difference}.
	\item Count \textbf{\textit{counts}} the \textbf{\textit{number}} of \textbf{\textit{occurrences}} a \textbf{\textit{specified}} element, such as if I had 6 of each one pound coins, it will \textbf{\textit{return}} 6 since there are \textbf{\textit{six}} element of one pound coins.
	\item sum performs additional operations based on the occurrences of that element from both multi-sets resulting an occurrence $(n + m)$

	\item \textbf{Equals} - The equals operations validates \textbf{\textit{if}} \textit{both} multi-sets are equal \textbf{\textit{if}} they contain the \textbf{\textit{same}} elements and \textbf{\textit{only if}} they appear the \textbf{\textit{same}} number of times, thus $A \subseteq B$ is true.
	
	\item \textbf{isSubbag()} - Similar to \textbf{\textit{isSubset}}, this validates if both multi-sets are sub-bag, meaning that all duplicated members of Multi-Set A is a member of Multi-Set B.
\end{itemize}

\section{Maps Collection}
A Map is referred to as an abstract data type, which uses a set a keys which \textbf{\textit{map}} to a set of values. Each key defined is \textbf{\textit{associated}} with a value to result a \textbf{\textit{key-value pair}}. Although keys must be unique to differentiate between them, we can have the same value, such example would be a phone-book, although we must all have a unique phone the person's can be the same.

\subsection{Operations}
Typical operations that are defined in a map are:
\begin{itemize}
	\item \textbf{Add} -  We can add a new value and associate it with a key.
	\item \textbf{Reassign} - We can change the value of an existing key, and then release the previous value.
	\item \textbf{Remove} - We can remove a key, and deference it from a set of key-valued pairs from a map.
	\item \textbf{Lookup} -  We can perform a lookup from a given key to get the value.
\end{itemize}

\subsection{Types and Usages of Maps}
Maps are also referred to as {\ttfamily{\color{blue}Dictionary}},  {\ttfamily{\color{blue}Associative Arrays}}, and  {\ttfamily{\color{blue}Lookup table}},  {\ttfamily{\color{blue}Hashtable}}.
Although {\ttfamily{\color{blue}Dictionary}}, and {\ttfamily{\color{blue}Associative Arrays}} are part of the Java standard library, they \textbf{\textit{should not}} be used as they are \textbf{\textit{obsolete}} meaning that they are \textbf{\textit{no longer}} maintained by the Java developers, and they have \textbf{\textit{highly}} suggested \textbf{\textit{not}} to extend \\the \textit{Dictionary/Associative Array} class, and \textbf{\textit{implement}} the \textit{Map} interface.
\\\\
Maps can be used to \textbf{\textit{model}} the \textbf{\textit{records}} of an address book, modelling \textbf{\textit{entries}} within a dictionary, records within a database, as well as \textbf{\textit{any}} table.

\chapter{Appendixes}
\section{StackADT<T> Interface}
\begin{lstlisting}[language=Java, numbers=left,  numberstyle=\footnotesize, keywordstyle=\color{blue},   commentstyle=\color{red}, basicstyle=\small\ttfamily]
public interface StackADT<T> {
	/**
	* This will insert the element into the stack from the top
	*/
    public void push(T element);
    
    /*
    * This will remove the element from the stack
    */
    public T pop();
    
    /**
    * This will return the top element within the stack
    */
    public T peek();
    
    /**
    * This will check if the stack is empty
    */
    public boolean isEmpty();
    
    /**
    * This will check the size of the stack.
    */
    public int size();
}
\end{lstlisting}

\subsection{Creating a BoundedStackADT<T> interface}
\begin{lstlisting}[language=Java, numbers=left,  numberstyle=\footnotesize, keywordstyle=\color{blue},   commentstyle=\color{red}, basicstyle=\small\ttfamily]
public interface BoundedStackADT<T> {
    /**
    * Returns true if the stack is full.
    */
    public boolean isFull();
    
    /**
    * This returns the size of the stack
    */
    public int capacity();
}
\end{lstlisting}

\section{QueueADT<T> Interface}
\begin{lstlisting}[language=Java, numbers=left,  numberstyle=\footnotesize, keywordstyle=\color{blue},   commentstyle=\color{red}, basicstyle=\small\ttfamily]
public interface Queue<T> {
    /**
    * This inserts elements at the back of the queue
    */
    public void enqueue(T element);
    
    /**
    * This removes the element from the queue, and returns the element from the front
    * of the queue.
    */
    public T dequeue();
    
    /**
    * Returns the first element from the queue.
    */
    public T first();
    
    /**
    * Checks if the queue is empty
    */
    public boolean isEmpty();
    
    /**
    * Returns the size of the queue.
    */
    public int size();
}
\end{lstlisting}

\section{ListADT<T> Interface}
\begin{lstlisting}[language=Java, numbers=left,  numberstyle=\footnotesize, keywordstyle=\color{blue},   commentstyle=\color{red}, basicstyle=\small\ttfamily]
public interface ListADT<T> {
    /**
    * Removes and returns the first element from the list.
    */
    T removeFirst();
    
    /**
    * Removes and returns the last element from the list.
    */
    T removeLast();
    
    /**
    * Removes a specific element from the list.
    */
    boolean remove(T element);
    
    /**
    * Returns a reference to the first element in the list.
    */
    T first();
    
    
    /**
    * Returns a reference from the last element from the list.
    */
    T last();
    
    boolean contains(T target);
    
    /**
    * Returns true if the list is empty
    */
    boolean isEmpty();
    
    /**
    * Returns the size of the list.
    */
    int size();
}
\end{lstlisting}

\section{SetADT<T> Interface}
\begin{lstlisting}[language=Java, numbers=left,  numberstyle=\footnotesize, keywordstyle=\color{blue},   commentstyle=\color{red}, basicstyle=\small\ttfamily]
public interface SetADT<T> extends Iterable<T> {
    public boolean add(T element);
    
    public boolean addAll(SetADT<T> A);
    
    public boolean remove(T element);
    
    public boolean removeAll(SetADT<T> A);
    
    public boolean retainAll(SetADT<T> A);
    
    public boolean contains(T element);
    
    public SetADT<T> union(SetADT<T> A);
    
    public SetADT<T> difference(SetADT<T> A);
    
    public SetADT<T> intersection(SetADT<T> A);
    
    public boolean equals(SetADT<T> A);
    
    public boolean isSubset(SetADT<T> A);
    
    public boolean isEmpty();
    
    public int size();
    
    //additional methods
    public T pick();
    
    public T removeRandom();
}
\end{lstlisting}

\section{MultiSetADT<T> Interface}
\begin{lstlisting}[language=Java, numbers=left,  numberstyle=\footnotesize, keywordstyle=\color{blue},   commentstyle=\color{red}, basicstyle=\small\ttfamily]
public interface MultiSetADT<T> extends Iterable<T> {
    public boolean add(T element);
		
    public boolean addAll(MultiSetADT<T> A);
    
    public boolean remove(T element);
    
    public boolean removeAll(MultiSetADT<T> A);
    
    public boolean retainAll(MultiSetADT<T> A);
    
    public boolean contains(T element);
    
    public MultiSetADT<T> union(MultiSetADT<T> A);
    
    public MultiSetADT<T> difference(MultiSetADT<T> A);
    
    public MultiSetADT<T> intersection(MultiSetADT<T> A);

    public MultiSetADT<T> sum(MultiSetADT<T> A);
    
    public boolean equals(SetADT<T> A);
    
    public boolean isSubbag(MultiSetADT<T> A);
    
    public boolean isEmpty();
    
    public int size();
}
\end{lstlisting}

\section{MapADT<T> Interface}
\begin{lstlisting}[language=Java, numbers=left,  numberstyle=\footnotesize, keywordstyle=\color{blue},   commentstyle=\color{red}, basicstyle=\small\ttfamily]
public interface MapADT<K,V> {
    public void add(K key, V value);
    
    public V remove(K key);
    
    public V lookup(K key);
    
    public V reassign(K key, V newValue);
}
\end{lstlisting}
% For ADT types, we must be careful with which type we are using as they might be implemented
%differently with different operations. The main focus is to implement pure ADTs.
\end{document}