\documentclass[a4paper, 11pt]{report}
\usepackage[pdftex, pdfauthor={Ibrahim Ahmad}, pdftitle={Implementing Queues and Stacks using Arrays}, pdfsubject={Data Structures and Algorithms}]{hyperref}

\usepackage{geometry}
\geometry{a4paper, margin=2.54cm}
\usepackage{bookmark}
\usepackage{listings}
\usepackage{graphicx}
\usepackage[T1]{fontenc}
\usepackage[many]{tcolorbox}

\usepackage{fancyhdr}
\pagestyle{fancy}

\usepackage{nimbusmono}
% Title Page
\title{Implementing Queues, Stacks, Sets using Arrays}
\author{Ibrahim Ahmad}


\begin{document}
	\chapter[Implementing ADTs]{Implement Stack, Queues, Sets using Arrays}
	\section{Main Focus}
	\begin{itemize}
		\item Aim is to revise the array concepts.
		\item Implement a stack that is both bounded and unbounded using an array.
		\item Implement a queue using an array.
		\item Examine an Array Implementation of a bounded set ADT.
		\item Examine time complexity from the above ADTs using Big-O notation.
	\end{itemize}
	
	\section{Working with Arrays}
	% TODO: \usepackage{graphicx} required
	\begin{figure}[h]
		\centering
		\includegraphics[width=0.7\linewidth]{arrays1}
		\caption{Represents an Array in a box-like structures. Arrays are kept continuously.}
		\label{fig:arrays1}
	\end{figure}
	Arrays are \textbf{\textit{represented}} as a \textit{box-like structure} which \textbf{\textit{start}} at zero as the first position, and they \textbf{\textit{end}} n - 1, where n is the size minus 1. Elements within an array are inserted, and kept \textbf{\textit{contiguously}}. Assume that we have an array of \textbf{\textit{size 10}}, it would have the range of \textbf{0 to 9}. \\\\
	Arrays can be easily \textbf{\textit{defined}} using \textit{primitive} types, or \textit{user-defined} types.
\begin{lstlisting}[language=java, numbers=left,  numberstyle=\footnotesize, keywordstyle=\color{blue},   commentstyle=\color{red}, basicstyle=\small\ttfamily]
int[] a;
//creates a new integer of size 10
a = new int[10];

//same can be achieved during initialisation
double[] b = new double[20];

//we can initialise the array with elements within curly-braces
int[] c = {1, 2, 4, 7};

//however, we cannot initialise an array with a set size and then
//initialise the element, that is declared as primitive
//as two operations cannot be completed at the same time.
//since it has been initialised
//
double[] d = new double[4] = {1.0, 2.0, 3.0, 4.0}; //not allowed

double[e] = new double[2];

//once array is initialised, array aggregates can only be used
double[0] = 1.0 //allowed!
\end{lstlisting}

\subsection{Arrays initialised}
In general, arrays are \textit{automatically} \textit{\textbf{initialised}} using a zero bit pattern. Arrays of primitive types are usually initialised with a value, such as an array of type {\ttfamily{int}} will be initialised to \textbf{\textit{zero}} and for an array of type {\ttfamily{boolean}} it will be initialised with \textbf{\textit{false}}. 
\\\\For array of \textbf{\textit{reference}} types they will be \textbf{\textit{initialised}} to null.

\subsection{Traversing or Accessing an Array}
\begin{lstlisting}[language=java, numbers=left,  numberstyle=\footnotesize, keywordstyle=\color{blue},   commentstyle=\color{red}, basicstyle=\small\ttfamily]
int[] arr = {2,4,6,8}

//We can access each element within an array using the final field length
//
for(int i = 0; i < arr.length; i++) {
	a[i] *= 2 //multiply each element by two
}

//Or we can iterate over an array using an enhanced for-loop
//
for(int a : arr) {
	a = -a; //change the sign value for each element
}

\end{lstlisting}
Arrays can be \textbf{\textit{accessed}} from a for-loop using a {\ttfamily{final}} field called length in order to \textbf{\textit{prevent}} a faulty, or an infinite loop from occurring.
It is possible to \textbf{\textit{iterate}} over an array using a \textit{for-each} loop, where the \textit{left-hand side} represents the \textbf{\textit{iterator}}, and the \textit{right-hand} side represents the \textbf{\textit{collection}} we are iterating over. 

\subsection{Time complexity of an Array}
The accessing time of an array is \textbf{\textit{constant}} time $O(1)$, which is quite fast, which only involves calculating address of the element stored within an array.
\\\\
Although accessing an array is constant, it involve \textbf{\textit{looping}} through an array at \textbf{\textit{n}} times, which results the time complexity to be \textbf{\textit{linear}} time $O(n)$ since we are \textbf{\textit{depending}} on \textbf{n} for \\ \textbf{\textit{determining}} the address of the element. 

\section[Bounded Stack]{Bounded Stack implementation}
The bounded stack implementation is a bounded ADT which uses an array to store elements. Meaning that once storage has been \textbf{\textit{allocated}} for the stack, it will \textbf{\textit{remain}} fixed size throughout the life of the program.
As the stack grows, it grows \textbf{\textit{upwards}} from the \textbf{\textit{start}} of the array when items as \textbf{\textit{pushed}} into the stack, the stack \textbf{\textit{shrinks}} when items are \textbf{\textit{popped}} off the stack.
\\\\
For the \textbf{\textit{Pop}} and \textbf{\textit{Push}} operation we need to \textbf{\textit{keep}} track of the position using an \textit{integer} variable.
\\\\
For the stack we need to use a {\ttfamily{\color{blue}clear}} operation to clear \textbf{\textit{all}} items from the stack.

\subsection{Structure of Bounded Stack}
\begin{lstlisting}[language=java, numbers=left,  numberstyle=\footnotesize, keywordstyle=\color{blue},   commentstyle=\color{red}, basicstyle=\small\ttfamily]
public class ArrayStack<T> implements BoundedStackADT<T> {
//we set the initial capacity of the array, since its fixed 
//it will be static
    private static final int DEFAULT_CAPACITY = 100;
	
//since the stack is bounded we will set a max size integer to final.
    private final int maxSize;
    private T[] elements; //the actual array
    private int currentSize; //keep track of size
    
    /**
    * This will permit the user to construct a stack
    * based on a defined size.
    * 
    * @param definedSize - This will set a defined size for the maximum
    * capacity of the stack
    */
    public ArrayStack(int definedSize) {
        this.elements = (T[]) (new Object[maxSize]);
        this.maxSize = definedSize();
        this.currentSize = 0;
    }
    
    /**
    * This will re-use the constructor method via this()
    * however it will set the stack with the default capacity.
    */
    public ArrayStack() {
        this(DEFAULT_CAPACITY); //this will set the stack to default capacity
    }

    public boolean isEmpty() {
    	return currentSize == 0;
    }
    
    public int capacity() {
    	return maxSize;
    }
    
    public int size() {
    	return currentSize;
    }
    
    public int boolean isFull() {
    	return currentSize == maxSize;
    }
    
    public void push(T element) {
    	if(isFull()) {
    		throw new IllegalStateException(
    		    "Cannot push element. Stack is full!"
    		);
    	}
    
        elements[currentSize] = element;
        currentSize++;
    	
    }
    
    public T pop() {
    	if(isEmpty()) {
    		throw new IllegalStateException(
    		    "Cannot pop element. Stack is empty!"
    		);
    	}
    
    	currentSize--;
    	return elements[currentSize];
    }
    
    public T peek() {
    	if(isEmpty()) {
    		throw new IllegalStateException(
    		    "Cannot peek an element. Stack is empty!"
    		);
    	}
    
    	//returns the top element which is at the end of the array
        return elements[currentSize - 1];
    }
    
    public void clear() {
    	currentSize = 0;
    }
} 
\end{lstlisting}

\begin{itemize}
	\item We have used two constructors, one is used to create a stack at a defined size, where the second one holds the default capacity.
	
	\item We have created an array of the generic type T, however when instantiating the array we must use the Object type and cast with the type of T. The reason why we need to cast, is because Java cannot create arrays of the generic type.
	
	\item There are four methods typically included and that is the {\ttfamily{\color{blue}isEmpty()}} which returns true if the currentSize is zero, a {\ttfamily{\color{blue}capacity()}} method which \textbf{returns} the \textbf{\textit{maxSize}}, the {\ttfamily{\color{blue}size()}} method which \textbf{returns} the \textbf{\textit{currentSize}}, and a boolean {\ttfamily{\color{blue}isFull()}} method which \textbf{\textit{returns}} true if the currentSize \textbf{\textit{equals}} the maximum capacity. 
	
	\item The \textbf{\textit{push()}} method is used to \textbf{\textit{insert}} an element at the top of the stack. \\Firstly, it \textbf{\textit{checks}} if the stack is full, if it \textbf{\textit{is full}} it simply \textbf{\textit{throw}} an exception, otherwise \textbf{\textit{stores}} the element, causing the stack to move \textbf{\textit{upwards}} and the currentSize will \textbf{\textit{grow}}.
	
	\item The \textbf{\textit{pop()}} method is used to \textbf{\textit{pop}} the element from the stack. \\It first \textbf{\textit{checks}} if the stack is empty, if true then it will \textbf{\textit{throw}} an exception, otherwise it will \textbf{\textit{decrease}} the size of the stack, and \textbf{\textit{return}} the element.
	
	\item The \textbf{\textit{peek()}} method will first check if the stack is empty, if true then it will throw the exception, otherwise it will return the item from the top of the stack.
	\\\\
	Please note that the \textit{\textbf{top}} element is located at the \textbf{\textit{end}} of the array.
	
	\item The \textbf{\textit{clear()}} method is responsible for clearing the stack by \textbf{\textit{setting}} the current size to zero, resulting the \textbf{\textit{current}} elements to become \textbf{\textit{inaccessible}} to the clients.
	
	\item According to the implementation of an {\ttfamily{\color{blue}ArrayStack<T>}}, we can see there is \textbf{\textit{no}} loop used in \textbf{\textit{all}} methods defined, resulting the stack's operations to be \textbf{\textit{executed}} in \textit{constant} time. $O(1)$ 
\end{itemize}

\section[Unbounded Stack]{Unbounded Stack implementation}
Arrays are \textbf{\textit{not}} usually the best option for ADTs, though it is possible to \textbf{\textit{create}} an \textbf{\textit{unbounded}} stack by \textbf{\textit{expanding}} the capacity of array by \textbf{\textit{creating}} a \textit{new} array with \textbf{\textit{twice}} the capacity, \textbf{\textit{populate}} it from the \textit{current} array, and then \textbf{\textit{overwrite}} the \textit{current} array.\\\\
This will implement the unbounded array stack interface defined earlier.

\subsection{Expanding the capacity of the array}
A way to achieve this is by expanding the array by simply an expand capacity method which simply creates a new array with twice of the previous array's capacity, and then populates the new array with the previous array, and then updates the previous array with the new array.
\\\\
Since this method is regarded as the implementation details, we set it to {\ttfamily{\color{blue}private}} to achieve abstraction.
\begin{lstlisting}[language=java, numbers=left,  numberstyle=\footnotesize, keywordstyle=\color{blue},   commentstyle=\color{red}, basicstyle=\small\ttfamily]
private void expandCapacity() {
    //1. Create a new array with twice the capacity
    T[] newArray = (T[]) (new Object[contents.length * 2]);
	
    //2. Populate data from previous array to new array
    for(
        int fillerIndex = 0;
        fillerIndex < contents.length;
        fillerIndex++ 
    ){
	    newArray[fillerIndex] = contents[fillerIndex];
	}
    
    //3.Update the previous array.
    this.contents = newArray;
}
\end{lstlisting}

\subsection{Push Operation}
For each \textbf{\textit{push()}} operation instead of \textbf{\textit{throwing}} an exception, we would simply \textbf{\textit{expand()}} the capacity of the array using the \textbf{\textit{expandCapacity()}} method that was defined.
\begin{lstlisting}[language=java, numbers=left,  numberstyle=\footnotesize, keywordstyle=\color{blue},   commentstyle=\color{red}, basicstyle=\small\ttfamily]
public void push(T element) {
    if(isFull()) {
	    expandCapacity();
    }
 
    contents[currentSize] = element;
    currentSize++;
}
\end{lstlisting}

\begin{itemize}
	\item According to \textbf{\textit{push()}} operation that was defined, once the array becomes \textbf{\textit{full}}, it will need to \textbf{\textit{trigger}} the \textbf{\textit{expandCapacity()}} method which will \textbf{\textit{result}} the time complexity to be $O(n)$. However, this is not always the case, as the array will \textbf{\textit{only}} expand its capacity when it \textbf{\textit{becomes}} full.
	
	\item We must \textbf{\textit{carefully}} select the \textbf{\textit{initial}} capacity to \textbf{\textit{avoid}} encountering the expandCapacity() method in order to \textbf{\textit{keep}} the execution time \textbf{\textit{constant}}.
\end{itemize}

\section{Bounded Queue}
We can \textbf{\textit{create}} a queue that can be \textbf{\textit{managed}} using indexes \textbf{\textit{starting}} from 0 to the end.
For as part of this implementation we need to \textbf{\textit{declare}} an integer pointer called \textbf{rear} to \textbf{\textit{keep}} track of the \textbf{\textit{next}} available slot in the queue as it moves downwards.

\subsection{Structure}
\begin{lstlisting}[language=java, numbers=left,  numberstyle=\footnotesize, keywordstyle=\color{blue},   commentstyle=\color{red}, basicstyle=\small\ttfamily]
public class ArrayQueue<T> implements QueueADT<T> {
    private static final int DEFAULT_CAPACITY = 100;
    private int rear;
    private T[] queue;
    
    public ArrayQueue(int initialCapacity) {
    	rear = 0;
    	queue = (T[]) (new Object[initialCapacity]);
    }
    
    public ArrayQueue() {
    	this(DEFAULT_CAPACITY);
    }
}
\end{lstlisting}
\begin{itemize}
	\item We \textbf{\textit{declare}} a field for holding the \textbf{\textit{default}} capacity which is \textit{100}, since this is the \textbf{\textit{default}} capacity we are going to set it as \textbf{\textit{static}} and \textbf{\textit{final}}.
	
	\item We \textbf{\textit{declare}} a rear field to \textbf{\textit{keep}} track of the \textbf{\textit{next}} available in the queue.
	
	\item Declare an \textbf{\textit{array}} of the generic type \textbf{\textit{T}} to \textbf{\textit{hold}} the contents in the queue.
	
	\item We declare two constructors one is to \textbf{\textit{initialise}} the queue based on a \textit{defined} capacity, along with \textbf{\textit{setting}} the rear pointer by zero since there is \textbf{\textit{no}} elements, the second constructor is used to \textbf{\textit{set}} the queue to the \textit{default} capacity.
	
	\item There are \textbf{\textit{four}} typical methods such as determining the \textbf{\textit{size}} of the queue from \textbf{\textit{rear}} pointer, if the queue is \textbf{\textit{empty}} in other words if the rear is zero. It also \textbf{\textit{checks}} if the queue is \textbf{\textit{full}}, and the \textbf{\textit{capacity}} of the queue. 
\end{itemize}

\subsection{Enqueue Operation}
\begin{lstlisting}[language=java, numbers=left,  numberstyle=\footnotesize, keywordstyle=\color{blue},   commentstyle=\color{red}, basicstyle=\small\ttfamily]
public void enqueue(T element){
    if(rear == size()) {
	    throw new IllegalStateException("Queue is now exceeding its capacity.");
    }
    
    queue[rear] = element;
    rear++;
}
\end{lstlisting}

\begin{itemize}
	\item The enqueue operation first checks if the queue is full, if so then it will throw an exception, otherwise it will insert the element to the queue, and update the rear pointer by one.
	
	\item Since there is \textbf{\textit{no looping}} involved the time complexity of this method is \textbf{\textit{constant}}. $0(1)$
\end{itemize}

\subsection{Dequeue}
\begin{lstlisting}[language=java, numbers=left,  numberstyle=\footnotesize, keywordstyle=\color{blue},   commentstyle=\color{red}, basicstyle=\small\ttfamily]
public void dequeue(){
    if(isEmpty()) {
        throw new IllegalStateException(
            "Queue is empty. Cannot proceed dequeue..."
        );
    }
	
    //get the first element, as we would return that
    T result = queue[0];
    rear--;
	
    //move the elements forwards by one, to get the successor as the top element.
    for(int shiftForwards = 0; shiftForwards < contents.length; shiftForwards++) {
    	queue[shiftForwards] = queue[shiftFowards + 1];
    }

    //set the top element to null
    queue[rear] = null;
    return result;
}
\end{lstlisting}

\begin{itemize}
	\item Firstly, we \textbf{\textit{must}} check if the queue is \textbf{\textit{empty}}, if so \textbf{\textit{throw}} an exception, otherwise we need to \textbf{\textit{store}} the \textit{first} element that is at the \textbf{\textit{back}} of the queue which is at index \textit{0}, and \textbf{\textit{shrink}} the queue size by \textit{one} from the \textbf{\textit{rear}}.
	
	\item We then \textbf{\textit{need}} to use a loop to \textbf{\textit{shift}} the elements \textit{one} place \textbf{\textit{ahead}}.
	
	\item Once we have \textbf{\textit{iterated}} over the elements, we then \textbf{\textit{set}} the element at the \textit{front} of the queue to \textbf{\textit{null}}, and \textbf{\textit{return}} the \textit{element} that was at the \textbf{\textit{back}} of the queue.
	
	\item Despite that we are \textbf{\textit{required}} to use a loop \textbf{\textit{back}} shift the elements, this results the time complexity to become \textbf{\textit{linear}} time, which is $O(n)$.
\end{itemize}

The problem with this approach is that queue is \textbf{\textit{capable}} of working at \textbf{\textit{both}} ends, resulting the elements which are stored in an array to be \textbf{\textit{shifted}} at one end from index 0, thus such implementation is considered as inefficient.

\subsection{Unbounded Queue}
An {\ttfamily{UnboundedArrayQueue}} can be used to \textbf{\textit{create}} a queue that can hold \textbf{\textit{unlimited}} to number of elements by \textbf{\textit{modifying}} the \textbf{\textit{enqueue()}} in the class ArrayQueue, so that the enqueue operation will be able to expand its capacity using {\ttfamily{\color{blue}expandCapacity()}} rather than using throwing an IllegalStateException.

\subsection{Abstract Set}
An Abstract set is an Abstract class that \textbf{\textit{holds}} the \textit{typical} operations \textbf{\textit{required}} from the \textbf{\textit{SetADT<T>}} interface, which is used when \textbf{\textit{implementing}} the ArraySet class. The AbstractSet class is used so that the \textbf{\textit{concrete}} subclasses will be \textbf{\textit{responsible}} for \textbf{\textit{implementing}} methods that are provided by the \textbf{\textit{Iterable}} interface. \\\\
This \textbf{\textit{reduces}} the implementation details for \textbf{\textit{implementing}} the sets.
\subsubsection{Interface for holding the set}
The SetADT<T> interface extends the Iterable<T> interface, so that the \textbf{\textit{concrete}} subclasses will use the methods that are \textbf{\textit{provided}} by the Iterator<T> interface in order to \textbf{\textit{iterate}} over the elements using the methods provided from the \textbf{\textit{Iterable<T>}} interface rather than the AbstractSet abstract class. 
\\\\
For instance, for the \textbf{\textit{addAll()}} method we could have simply used the \textbf{\textit{add()}} method to add elements one by one using a for loop, however we can \textbf{\textit{iterate}} over elements using \textit{iterators}.
\\\\
The removeAll() can also be used to \textbf{\textit{remove}} element similar to the remove().
\\\\
It is important that the processes defined are completely independent\footnote{Completely independent means that despite the processes which \textbf{\textit{implement}} the Iterable interface, the \textbf{\textit{properties}} for each process is \textbf{\textit{independent}}.} for the details provided for each variant of set.
\paragraph{contains()} With the contains() method we are able to \textbf{\textit{iterate}} over each element to determine if \textbf{\textit{every}} element is a member of set A. 
\paragraph{isSubset()} The isSubset() is implemented \textbf{\textit{similar}} to the contains() method, in terms of \textbf{\textit{checking}} if \textbf{\textit{all}} elements within set A is in set B.
However, the methods defined will be \textbf{\textit{independent}} of the details of how contains(), and iterator() is \textit{implemented}.
\\\\
However, we are still able to \textit{\textbf{implement}} the equals() method using the \textbf{\textit{isSubset()}}, to achieve this we would \textbf{\textit{compare}} the mathematical result to check \textbf{\textit{if}} and \textbf{\textit{only}} if $A \subseteq B$ and $B \subseteq A$.
\\\\
The use of an abstract class will therefore \textbf{\textit{simplify}} the implementation details of an ADT interface, which is \textbf{\textit{mostly}} used using the Java Collections Framework.
\subsection{Structure of Abstract Set}
\begin{lstlisting}[language=java, numbers=left,  numberstyle=\footnotesize, keywordstyle=\color{blue},   commentstyle=\color{red}, basicstyle=\small\ttfamily]
public abstract class AbstractSet<T> implements SetADT<T> {
    
    public boolean addAll(SetADT<T> A);
    
    public boolean removeAll(SetADT<T> A) {}
    
    public boolean retainAll(SetADT<T> A) {}
    
    public boolean isSubset(SetADT<T> A) {}
    
    public boolean equals(SetADT<T> A) {}
    
}
\end{lstlisting}
\subsection{Remarks}
\begin{itemize}
\item The \textbf{\textit{addAll()}} method uses a for-each statement to \textbf{\textit{insert}} elements from the set \textbf{\textit{one-by-one}} to the \textit{current} set, using the \textbf{\textit{add}} method whilst \textbf{\textit{validating}} if the element has been \textbf{\textit{successfully}} \textit{inserted} by setting a \textbf{\textit{boolean}} flag for determining if the set has been modified. \footnote{Since the add method is a boolean method, it checks if there is \textbf{\textit{no ambiguity}} of the element, if so the element should be inserted successfully whilst checking if the element exists in the set using the \textbf{\textit{contains()}} method.}
\begin{lstlisting}[language=java, numbers=left,  numberstyle=\footnotesize, keywordstyle=\color{blue},   commentstyle=\color{red}, basicstyle=\small\ttfamily]
public boolean addAll(SetADT<T> a) {
    boolean isModified = false;    
    for(T element : a) {
       if(this.add(element)){
           isModified = true;       
       }
    }
    
    return isModified; 
}
\end{lstlisting}
\item The \textbf{removeAll()} method uses an \textbf{\textit{Iterator}} in order to \textbf{\textit{iterate}} over each element by \textbf{\textit{validating}} if the element can be \textbf{\textit{removed}} successfully from the set by \textit{\textbf{setting}} a boolean flag for determining if the set has been \textbf{\textit{modified}}. \footnote{Since the remove() method returns a boolean value, it will check if the element exists in set.}
\begin{lstlisting}[language=java, numbers=left,  numberstyle=\footnotesize, keywordstyle=\color{blue},   commentstyle=\color{red}, basicstyle=\small\ttfamily]
public boolean removeAll(SetADT<T> a) {
    boolean isModified = false;
    Iterate<T> iter = a.iterator();    
    while(iter.hasNext()) {
       if(this.remove(iter.next())){
           isModified = true;       
       }
    }
    
    return isModified; 
}
\end{lstlisting}
	
\item The retainAll() method uses an \textbf{\textit{Iterator}} to iterate over the elements, and stores the current element that is iterate using an iterator in a temporary variable to \textbf{\textit{validate}} if set A does not contain the \textbf{\textit{target}} element from set B, if so it will \textbf{\textit{remove}} the element from the set and it is \textbf{\textit{assigned}} to the boolean flag to determine if elements from set B have been \textbf{\textit{retained}} successfully from set A. 
\begin{lstlisting}[language=java, numbers=left,  numberstyle=\footnotesize, keywordstyle=\color{blue},   commentstyle=\color{red}, basicstyle=\small\ttfamily]
public boolean retainAll(SetADT<T> a) {
    boolean isModified = false;
    Iterate<T> iter = a.iterator();    
    while(iter.hasNext()) {
       T element = iter.next();
       if(!a.contain(element)){
           isModified = this.remove(element);       
       }
    }
    
    return isModified; 
}
\end{lstlisting}	
\item \textbf{\textit{isSubset()}} method firstly checks if the current set \textbf{\textit{depicted}} as \textbf{\textit{this}} is \textit{larger} than setA, if so we \textbf{\textit{return}} \textit{false}, as set A must be \textbf{\textit{larger}} than the current set, otherwise it will use a \textbf{\textit{for-each}} loop to \textbf{\textit{iterate}} over the elements to validate if set A \textbf{\textit{does not}} contains the element, if so it will \textbf{\textit{return}} \textit{false} \footnote{This will check if the element within set A does not exist, as an element that does not exist means that the set is not a subset of set A.}
\begin{lstlisting}[language=java, numbers=left,  numberstyle=\footnotesize, keywordstyle=\color{blue},   commentstyle=\color{red}, basicstyle=\small\ttfamily]
public boolean isSubset(SetADT<T> a) {
    if(this.size() > a.size()) {
        return false;
    }

    for(T element : a) {
        if(!a.contains(element)) {
            return false;
        }        
    }    
    
    return true; 
}
\end{lstlisting}
\item \textbf{\textit{equals()}} method firstly checks if \textbf{\textit{both}} sets including the current set \textbf{\textit{do not}} have an \textbf{\textit{equal}} number of elements as set A \footnote{We cannot two sets equal if their sizes differ.}, if so we then \textbf{\textit{return}} \textit{false}, otherwise we would \textbf{\textit{return}} if set A \textit{is} a subset, and the current set \textit{is} a subset. \footnote{This check validates if both sets are subsets.}
\begin{lstlisting}[language=java, numbers=left,  numberstyle=\footnotesize, keywordstyle=\color{blue},   commentstyle=\color{red}, basicstyle=\small\ttfamily]
public boolean equals(SetADT<T> a) {
    if(a.size != this.size()) {
        return false;    
    }
    
    return isSubset(this) && isSubset(a);
}
\end{lstlisting}
\end{itemize}

\section{Bounded Set}
The \textbf{\textit{implementations}} of the \textit{constructor} and \textit{accessor} methods provided by the \textbf{\textit{BoundedStackADT}} interface such as the \textbf{\textit{isEmpty()}} method to \textbf{\textit{validate}} if the set is \textbf{\textit{empty}}, the \textbf{\textit{size}} of the set using the \textbf{\textit{size()}} method, and the \textbf{\textit{maximum}} capacity of the set using \textbf{\textit{capacity()}} method.
\subsection{Remarks}
When implementing methods such as union(), intersection(), difference() which holds the parameter variable of type Set<ADT> we must not assume that A is not an instance of ArraySet, as it only uses methods declared in SetADT<T> to ensure that those methods work, even if the methods are then overridden from the ArraySet. However, assume that the current set referred to this is an instance of ArraySet.
\\\\
Although, it is not the case with ArraySet, more efficient implementations of a set operation such as union, or isSubset do exist, if we wish to determine that both sets are involved of the same concrete class such as XSet, where X can have the implementations of a set using a Linked-list, array, tree or hash table. 
\\\\If that is the case, then the union() method would need to be overloaded, including the method signature of SetADT<T>, we would need to define a method associated with the Method signature, such as union(XSet<T> A).
For instance:
\begin{lstlisting}[language=java, numbers=left,  numberstyle=\footnotesize, keywordstyle=\color{blue},   commentstyle=\color{red}, basicstyle=\small\ttfamily]
public SetADT<T> union(ArraySet<T> A) {
    //Implementation detail omitted.
}
\end{lstlisting}

\subsection{Class Structure of ArraySet}
\begin{lstlisting}[language=java, numbers=left,  numberstyle=\footnotesize, keywordstyle=\color{blue},   commentstyle=\color{red}, basicstyle=\small\ttfamily]
public class ArraySet<T> extends AbstractSet<T> implements Bounded {
    private static final int DEFAULT_CAPACITY = 100;
    
    private T[] contents;
    private final int maxSize;
    private int currentSize;
    
    public ArraySet(int maxSize) {
    	contents = (T[]) (new Object[maxSize]);
    	currentSize = 0;
    	this.maxSize = maxSize;
    }

    public ArraySet() {
    	this(DEFAULT_CAPACITY);
    }

	public int size() {}
	
	public boolean isEmpty() {}
	
	public boolean capacity() {}
	
	public boolean isFull() {}
	
	public boolean contains(T element) {}
	
	public boolean add(T element) {}
	
	public boolean remove(T element) {}
	
	public SetADT<T> intersection(SetADT<T> A) {}
	
	public Iterator<T> iterator() {}
}
\end{lstlisting}
\begin{itemize}
	\item We have a field which holds the default capacity of the array which is set by default 100.
	
	\item We have three fields, one is used to holds the elements within an array of generic type T, the maximum size set for the set, and the current size for keeping track of the elements stored within the set.
	
	\item We have two constructors, one is used to set the set with a defined capacity, and set the current size of the set to zero, and the array is initialised to the type Object, whilst cast to the generic type T.
	\item We have four typical methods for a collection, one checks if the set is empty, the second returns the capacity of the array, the third checks if the set is full, and the fourth array returns the size of the set.
	\item The contains(T element) method is responsible for searching if the element is present within the array, if so it returns true, otherwise false.
\begin{lstlisting}[language=java, numbers=left,  numberstyle=\footnotesize, keywordstyle=\color{blue},   commentstyle=\color{red}, basicstyle=\small\ttfamily]
public boolean contains(T element) {
    for(int i = 0; i < currentSize; i++) {
       if(this.equals(element)){
           return true;       
       }
    }
    
    return false; 
}
\end{lstlisting}
\item Although we only need to search through the occupied portion of the array up to the current index of currentSize, we do not need to use a for loop search through the whole array.\\\\Although search through an array by a given index is faster, using an Iterator will only slow down the operation, and will not add any value to the implementation.
\item The add(T element) method is responsible for checking if the element exists in the set using the contains method, if so then it will return false, otherwise it will insert the element in the same way as push, and return true \footnote{If the add() method returns true, it will then indicate that the set has been modified by checking if the element exists in the set using the contains() method.}.
\begin{lstlisting}[language=java, numbers=left,  numberstyle=\footnotesize, keywordstyle=\color{blue},   commentstyle=\color{red}, basicstyle=\small\ttfamily]
public boolean add(T element) {
    if(!this.contains(element)) {    	   
       if(!isFull()) {
           this.contents[currentSize] = element;
           this.currentSize++;
           
           return true;
       }
       
       else {
           throw new IllegalStateException("Set is now full.");
       }
    }
    
    return false; 
}
\end{lstlisting}
\item The remove(T element) method is responsible of removing elements by looping through the array to see if the element exists. If we have located the element within the set, we simply decrease the currentSize of the set by one, then copy the last element to the i-th position to avoid leaving a gap in the array, and return true, otherwise we return false.
\begin{lstlisting}[language=java, numbers=left,  numberstyle=\footnotesize, keywordstyle=\color{blue},   commentstyle=\color{red}, basicstyle=\small\ttfamily]
public boolean remove(T element) {
    for(int i = 0; i < currentSize; i++) {
        if(element.equals(this.contents[i])) {
            this.currentSize--;
            this.contents[i] = this.contents[currentSize];
            return true;         
        }    
    }
    
    return false; 
}
\end{lstlisting}
	\item The intersection()  method simply generates a new set to hold the results of the set intersection operation. This is by traversing through each element of the current set, and then checking if it is an element of A, if so then we would store the i-th element that matches set A from the current set to the current position of the new set using the new set's currentSize.
	\item Alternatively we can simply insert the element using the add() provided by the ArraySet, however this would be inefficient as this would invoke not only the add() method with a for-loop, but the contains() method which also contains a for-loop resulting the asymptotic time complexity to become $O(n^3)$, as we would need to perform a method invocation to not only add, but also the contains(). 
	\\\\Although we know the element is not in the result set, so we insert it at the end of the set. 
\end{itemize}

\subsection{Array Iterator}
An array iterator is an array which is capable of traversing through all elements within an array by implementing the {\ttfamily{Iterable<T>}} interface. The iterator determines the order of each element in the array. If there is no logical order to its element, they are traversed randomly.
\subsubsection{Iterator method}
\begin{itemize}
	\item The \textbf{\textit{iterator()}} returns an Iterator object when traversing through the elements. The \textbf{\textit{Iterable}} interface is part of the {\ttfamily{java.lang}} library, however the {\ttfamily{Iterator}} class is part of the {\ttfamily{java.util}}.
	
	\item Since Java 8, the Iterator class object has four additional methods such as the hasNext() method which returns a boolean method if there are more elements to traverse in the collection.
	
	\item The next() returns the next element in the iteration.
	
	\item The {\ttfamily{default}} remove() method removes the current element of the collection, however by {\ttfamily{default}} it simply throws an UnsupportedOperationException, as it is unsupported.
	
	\item The {\ttfamily{default}} forEachRemaining() method simply performs an action using lambda expressions for each element remaining action that was processed, or throws an exception.
\end{itemize}

\subsubsection{The structure of ArrayIterator<T>}
\begin{lstlisting}[language=java, numbers=left,  numberstyle=\footnotesize, keywordstyle=\color{blue},   commentstyle=\color{red}, basicstyle=\small\ttfamily]
import java.util.Iterator;

public class ArrayIterator<T> implements Iterator<T> {
    private T[] contents;
    private int cursor;
    private int limit;
    
    public ArrayIterator(T[] array, int size) {
    	this.contents = array;
    	this.cursor = 0;
    	this.limit = size;
    }
    
    public boolean hasNext() {}
    
    public T next() {}
    
    public void remove() {}
    
}
\end{lstlisting}

\paragraph{Fields} We only need three fields, one is to hold the contents within the array, the cursor when iterating over the loop, the limit will simply hold the max size of the array.

\paragraph{Constructor} The constructor simply sets contents to the array we will be working with externally from the parameter variable array of generic type {\ttfamily{T[]}}, set cursor to zero, as we start from zero, and set the limit to the size int parameter variable.

\paragraph{hasNext()} This simply returns a boolean value if the cursor is less than limit, to determine there is any next value to traverse.

\paragraph{next()} This will simply return the value of generic type T, by checking if the iterator does not have the next element to iterate, if so it will throw an exception, otherwise return the element from contents, and increment cursor by one.

\paragraph{remove()} Since arrays cannot remove element since it is not an implementation choice, we simply throw an {\ttfamily{IllegalStateException}}. If the {\ttfamily{remove}} method was a choice it will result potential integrity issues in {\ttfamily{ArrayIterator}} which can affect the number of elements stored in a collection from the {\ttfamily{size()}}.

By implementing {\ttfamily{Iterable}} in {\ttfamily{ArrayIterator}}, the implementation of \textbf{\textit{iterable()}} is simple, as we only need to return a new instance of ArrayIterator<T>.
\begin{lstlisting}[language=java, numbers=left,  numberstyle=\footnotesize, keywordstyle=\color{blue},   commentstyle=\color{red}, basicstyle=\small\ttfamily]
import java.util.Iterator;

public Iterator<T> iterator() {
    return new ArrayIterator<T>(elems, currentSize);
}
\end{lstlisting}

\section{Time Complexity of ArraySet Operations}
According to the time complexity of the contains() method, it contains a loop to check if the element exists in the set, if so it will \textbf{\textit{execute}} \textbf{\textit{n }}times if the element is not present, where \textbf{\textit{n}} is the number of elements. Otherwise, if the element was there the average time complexity would be {$\frac{n}{2}$}.
\\\\
The add() method calls the contains() which firstly checks if they array is full, and the compares between two assignments, thus the time complexity is $0(n)$.
\\\\
The remove() method contains a loop, where the size of the set is n, the loop will be executed n time if the element is not present resulting $O(n)$ otherwise $O(\frac{n}{2})$ if the element was located.
\\\\
The time complexity of intersection() which intersects another instance of {\ttfamily{ArraySet}} is $O(n^2)$, since the intersection() is $O(n)$ which performs an invocation method call to the contains() to check if the element exists which is O(n).
\\\\
Both {\ttfamily{union()}} and {\ttfamily{difference()}} have the time complexity of quadratic time which is $O(n^2)$.
%Stacks and Queues do not need use iterator because they do not implement Iterable, and they do not remove elements by setting them to null as we do not want any gaps in them.
\end{document}          
